#!/usr/bin/env python3

import argparse
import gitlab
from datetime import datetime
import csv
import os

def read_allowed_list(filepath):
    allowed_users = set()
    with open(filepath,"r") as allowed_list:
        for line in allowed_list.readlines():
            allowed_users.add(line.lower().strip())
    return allowed_users

parser = argparse.ArgumentParser(description='Deactivate all inactive users')
parser.add_argument('gitlaburl', help='URL of the GitLab instance')
parser.add_argument('token', help='Admin API token to read the requested users')
parser.add_argument('--dryrun','-d', help='Only output list of users that would be deactivated without actually deactivating', action="store_true")
parser.add_argument('--no-dryrun', help='Do deactivations', dest='dryrun', action='store_false')
parser.add_argument('--allowed-list', help='List of accounts that will not be deactivated, one username per line.')
parser.set_defaults(dryrun=True)
args = parser.parse_args()

gitlaburl = args.gitlaburl if args.gitlaburl.endswith("/") else args.gitlaburl + "/"
gl = gitlab.Gitlab(gitlaburl, private_token=args.token)

do_dryrun = args.dryrun

allowed_list_file = args.allowed_list
allowed_users = set()
if allowed_list_file:
    allowed_users = read_allowed_list(allowed_list_file)


if do_dryrun:
    print("This is a dry run, no user accounts will be deactivated. Run with --no-dryrun to do deactivations.")

try:
    users = gl.users.list(as_list=False)
except Exception as e:
    print("Can not retrieve list of users: "+ str(e))
    exit(1)

tz = datetime.now().astimezone().tzinfo

deactivated = []

for user in users:
    if user.attributes["state"] != "active":
        continue
    if "current_sign_in_at" not in user.attributes:
        print("Cannot access user field 'current_sign_in_at', use an admin token.")
        exit(1)
    else:
        if ( user.attributes["username"] not in allowed_users ):
            current_sign_in = user.attributes["current_sign_in_at"]
            last_activity = user.attributes["last_activity_on"]
            deactivate = True
            if current_sign_in is not None and last_activity is not None:
                current_sign_in_dt = datetime.strptime(current_sign_in, '%Y-%m-%dT%H:%M:%S.%f%z')
                last_activity_dt = datetime.strptime(last_activity, '%Y-%m-%d')
                delta_sign_in = datetime.now(tz) - current_sign_in_dt
                delta_activity = datetime.now() - last_activity_dt
                if delta_sign_in.days < 90 or delta_activity.days < 90:
                    deactivate = False
            if deactivate:
                if not do_dryrun:
                    try:
                        print("Deactivating %s" % user.attributes["username"])
                        user.deactivate()
                        deactivated.append(user.attributes)
                    except Exception as e:
                        print("Could not deactivate %s: %s" % (user.attributes["username"], str(e)))
                else:
                    deactivated.append(user.attributes)


if not do_dryrun:
    print("Deactivated %s users, writing report." % str(len(deactivated)))
else:
    print("There are %s users to deactivate, writing report." % str(len(deactivated)))

reportfilepath = "report/deactivated_users_%s.csv" % str(datetime.now().date())
if do_dryrun:
    reportfilepath = "report/users_to_be_deactivated_%s.csv" % str(datetime.now().date())
os.makedirs(os.path.dirname(reportfilepath), exist_ok=True)

with open(reportfilepath, "w") as reportfile:
    reportwriter = csv.writer(reportfile, delimiter="\t", quotechar='"', quoting=csv.QUOTE_MINIMAL)
    fields = ["username","name","email","current_sign_in_at","last_activity_on"]
    reportwriter.writerow(fields)
    for user in deactivated:
        row = []
        for field in fields:
            row.append(user[field])
        reportwriter.writerow(row)

